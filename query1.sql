---FIND EMPLOYEE DETAILS HAVING SALARY EQUAL TO DEPARTMENT AVERAGE SALARY
---USING ANALYTICAL---USING SUBQUERY
SELECT E.* FROM (SELECT B.*,AVG(SALARY)OVER(PARTITION BY DEPARTMENT_ID) AVG FROM EMP B)E WHERE E.SALARY =E.AVG ;
---USING SUBQUERY
select * from 
emp 
where (department_id,salary) IN (select department_id,AVG(salary) from emp group by department_id);
--USING JOINS
SELECT E.EMPLOYEE_ID,E.FIRST_NAME,E.SALARY,B.AVG_SAL,B.DEPARTMENT_ID FROM EMP E JOIN 
(SELECT DEPARTMENT_ID,AVG(SALARY) AVG_SAL 
FROM EMP GROUP BY DEPARTMENT_ID)B ON E.DEPARTMENT_ID=B.DEPARTMENT_ID WHERE E.SALARY=B.AVG_SAL;
---FIND EMPLOYEE DETAILS HAVING SALARY GREATER THATN DEPARTMENT AVERAGE SALARY
---USING ANALYTICAL ---USING SUBQUERY
SELECT E.* FROM (SELECT B.*,AVG(SALARY)OVER(PARTITION BY DEPARTMENT_ID) AVG FROM EMP B)E WHERE E.SALARY >E.AVG ;
---USING CORRELATED SUBQUERY
select * from emp a where a.salary >(select avg(salary) from emp b where b.department_id=a.department_id);
---USING JOINS
SELECT E.EMPLOYEE_ID,E.FIRST_NAME,E.SALARY,B.AVG_SAL,B.DEPARTMENT_ID FROM EMP E JOIN 
(SELECT DEPARTMENT_ID,AVG(SALARY) AVG_SAL 
FROM EMP GROUP BY DEPARTMENT_ID)B ON E.DEPARTMENT_ID=B.DEPARTMENT_ID WHERE E.SALARY>B.AVG_SAL;
-----largest department(having maximum number of employees)
select * from (select department_id,count(employee_id) over (partition by department_id )
as ct from employees order by ct desc) where rownum=1
----rownum
select max(rownum) from emp;?total number of rows
select rownum,emp.* from emp;?gnerate one more column starting from 1,2,3
?

select row_number()over(order by employee_id),emp.* from emp; ?gnerate one more column starting from 1,2,3
?
