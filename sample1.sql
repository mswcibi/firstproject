CREATE OR REPLACE PROCEDURE welcome_msg1 (p_name IN VARCHAR2) 
IS
BEGIN
dbms_output.put_line ('Welcome '|| p_name);
END;